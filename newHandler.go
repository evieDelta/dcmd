/*
Package dcmd is a highly WIP early alpha library created around discordgo in order to provide more middleware functionalities, or more specifically to provide the functionality for routing, handling and creating commands for discord bots.
Primarily created for personal use though its hoped to be flexible enough to not just be limited to that.

Current features include the handling and routing of commands and subcommands, handling of multiple prefixes, and assist methods for creating commands.

Note that this library and all its documentation is still WIP and pre-release so things may change without notice and compatibility is likely to be broken frequently.
*/
package dcmd

import (
	"fmt"
	"time"

	"codeberg.org/eviedelta/dcmd/etc"
	"github.com/bwmarrin/discordgo"
	"github.com/patrickmn/go-cache"
)

// Possible TODO, if the "Inharetables" settings thing mentioned in types is done, than add that to this

// HandlerConfig , Configuration used when setting up a new handler with dcmd.New
type HandlerConfig struct {
	Prefixes []string // Global Prefixes to use when recognising commands | TODO add server specific prefix settings
	Man      []string // Manual, used for bot level help stuff, and in general to be used for the hand written help
	Admins   []string // A list of Discord user IDs to be considered Bot admins and be able to use commands specific to only the administrator of the bot
	Name     string   // The name of the bot :D (somewhere along the line if this is unset it might be set to a mention of the bot but i forget)

	GlobalLogWebhook string // A string of the webhook to use for logging errors and stuff, possibly to be replaced later with something that can just take a webhook url

	DeletePermErrorMessages bool // Delete permission error messages after a delay?
	DeleteErrorMessages     bool // Delete error messages after a delay?
	AutoDeleteDelay         int  // The delay in seconds to delete messages marked by above in

	// React or reply to certain events
	// Note for etc.Trit type use 3 for True and 2 for False
	ReactOn               ReactOn  // Events to react via emote reactions to
	ReplyPermissionErrors etc.Trit // Reply if a user doesn't have sufficient permissions?
	ReplyErrors           etc.Trit // Reply if an error happens preventing the command from working right (this contains the actual error message)

	PermissionErrorEmote string // Emote to react with if a user lacks permissions
	ErrorEmote           string // Emote to react with if an error happens
	SuccessEmote         string // Emote to react with if the command succeeds

	DefaultCmds DefaultOptions // The settings relating to default commands such as help and manual
}

// NewHandler Takes a Handler config and returns a new handler to use
// add commands with handler.AddCommand() or add commands in bulk from an array with handler.BulkAddCommands()
func NewHandler(cfg HandlerConfig, s *discordgo.Session) *RefHandler {
	cfg = hCfgCheckAndDefault(cfg)
	han := &RefHandler{
		Cfg:      cfg,
		Cache:    newCache(s),
		cmds:     make(map[string]bool),
		commands: make(map[string]Cmd),

		await: cache.New(5*time.Minute, 1*time.Minute),
	}
	han.await.OnEvicted(cancelAwait)
	return han
}

// HCfgCheckAndDefault Checks if any config values are unset and sets them to the defaults
func hCfgCheckAndDefault(cfg HandlerConfig) HandlerConfig {
	if len(cfg.Prefixes) < 1 {
		fmt.Println("DCMD Internal Notice: No default prefix set, you can use the bot without a prefix however in this case you'd want to use handler.AddAtMentionAsPrefix() to allow using an @ mention as a prefix")
	}
	if len(cfg.Admins) < 1 {
		fmt.Println("DCMD Internal Notice: No bot admins set, nobody will be able to use commands configured to only be used by the bot admins")
		// Bot admins in this context being a whitelist of users (typically the developers of the bot) who are trusted with permissions for special commands that only the developers of the bot should be able to use (things like setting the bot status for example)
		// you can set a bot admin via putting a discord User ID in the ``admins []string`` section of your dcmd.HandlerConfig")
		// to find your own user ID you can either print the .Author.ID section of the discordgo.MessageCreate event to the terminal and send a message with the bot running")
		// Or more preferably by turning on discord developer mode (Settings > Appearence > Developer mode) and copying the ID of your profile (right click on your username)")
	}
	if cfg.AutoDeleteDelay == 0 {
		cfg.AutoDeleteDelay = 10 // default is 10 seconds if unset
	}
	if cfg.PermissionErrorEmote == "" {
		cfg.PermissionErrorEmote = "❌" // an X emote in unicode
	}
	if cfg.ErrorEmote == "" {
		cfg.ErrorEmote = "⚠️" // Warning sign emote
	}
	if cfg.SuccessEmote == "" {
		cfg.SuccessEmote = "✅" // CheckMark emote
	}
	return cfg
}

package dcmd

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"github.com/patrickmn/go-cache"

	con "codeberg.org/eviedelta/dcmd/consts"
	"codeberg.org/eviedelta/dcmd/etc"
)

// TODO figure out which bits of legacy shenanigans can be removed
// TODO figure out which methods don't need to be exported, and then unexport them
// TODO add/fix comments and write documentation

// TODO Fully Implement Permissions

// TODO allow guild specific prefixes / guilds changing the prefix
// Possible feature, allow disabling commands on a guild level

// Far off future feature idea (for the sake of scope creep, work through all the other TODO and bugs and make what is already done stable before starting this)
// Add in a "reaction handler" to allow commands to use reactions as a means of input (similar to ctx.AwaitNextMessage but reactions)
// and maybe also add emote menu support to allow calling a specific function when a specific reaction is added to a message

// RefHandler the type for the entire system, contains the data of the handler and all the methods on the handler
type RefHandler struct {
	Cfg   HandlerConfig
	Cache *pCache

	cmds     map[string]bool
	commands cmdMap

	lwebh *discordgo.Webhook

	session *discordgo.Session

	await *cache.Cache
}

// SetSession updates the internal discordgo session used by some things
func (h *RefHandler) SetSession(s *discordgo.Session) {
	h.session = s
	h.Cache.dg = s
}

func (h *RefHandler) Name() string {
	if h.Cfg.Name != "" {
		return h.Cfg.Name
	}
	return h.session.State.User.Username + "#" + h.session.State.User.Discriminator
}

func (h *RefHandler) DefaultCfg() DefaultOptions {
	return h.Cfg.DefaultCmds
}

func (h *RefHandler) HasPrefix(msg string) bool {
	if etc.CheckPrefix(msg, h.Cfg.Prefixes) < 0 {
		return false
	}
	return true
}

func (h *RefHandler) GetPrefixes() []string {
	return h.Cfg.Prefixes
}

func (h *RefHandler) Description() string {
	return h.Cfg.Man[0]
}

func (h *RefHandler) Manual(page int) string {
	return h.Cfg.Man[page]
}

func (h *RefHandler) ManualPages() int {
	return len(h.Cfg.Man)
}

func (h *RefHandler) DoesCommandExist(key string) bool {
	return h.cmds[key]
}

func (h *RefHandler) AddCommand(cmd Cmd) {
	h.cmds[cmd.Name()] = true
	cmd.parentHandler(h)
	h.commands[cmd.Name()] = cmd
}

func (h *RefHandler) BulkAddCommands(cmd []Cmd) {
	for _, x := range cmd {
		h.AddCommand(x)
	}
}

func (h *RefHandler) FetchCmd(key []string) (Cmd, msgReturn) {
	//fmt.Println("handler fetchCMD")
	if chkDefault(key, h.Cfg.DefaultCmds) {
		//fmt.Println("Handler Builtin")
		return &RefCommand{}, msgReturn{Code: con.DefaultRoot, Args: key}
	}
	ke := strings.ToLower(key[0])
	if !h.DoesCommandExist(ke) {
		//fmt.Println("Handler no exist")
		return nil, msgReturn{Code: con.NoExist, Args: key}
	}
	//fmt.Println("handler passing onto command fetchcmd")
	return h.commands[ke].fetchCmd(key[1:])
}

func (h *RefHandler) awaitHandler(m *discordgo.MessageCreate) bool {
	// create cache key with author ID and channel ID and then fetch the await status
	key := genAwaitString(m.Author.ID, m.ChannelID)
	x, found := h.await.Get(key)
	if !found {
		return false
	}
	y := x.(*awaitState)

	// check for prefix and see if a user has given the cancel command (yeah its a lil jank putting a command outside of the command handler like this but it works)
	if a := etc.CheckPrefix(m.Content, h.Cfg.Prefixes); a > 0 {
		b := strings.ToLower(m.Content[a:])
		if strings.HasPrefix(b, "cancel") {
			h.await.Delete(key)
			return true
		}
	}

	// Generate the awaitData to pass to the channel
	args := etc.SplitArgs(m.Content, " ")
	data := AwaitData{Args: args}
	y.channel <- data
	y.done = true
	h.await.Delete(key)
	return true
}

func cancelAwait(key string, value interface{}) {
	x := value.(*awaitState)
	ch := x.channel
	if !x.done {
		ch <- AwaitData{Cancel: true}
	}
	close(ch)
}

func (h *RefHandler) GetCommandDesc(key string) string {
	if !h.DoesCommandExist(key) {
		return ""
	}
	return h.commands.description(key)
}

func (h *RefHandler) GetCommandManual(key string, page int) string {
	if !h.DoesCommandExist(key) {
		return ""
	}
	return h.commands.manual(key, page)
}

func (h *RefHandler) GetCommand(key string) Cmd {
	if !h.DoesCommandExist(key) {
		return nil
	}
	a := h.commands[key]
	return a
}

func (h *RefHandler) ListCommands() []string {
	return h.commands.commandList()
}

func (h *RefHandler) isUserBotAdmin(u string) bool {
	for _, x := range h.Cfg.Admins {
		if u == x {
			return true
		}
	}
	return false
}

func (h *RefHandler) CheckPermissions(m *discordgo.MessageCreate, p Permissions) (bool, int) {
	if p.BotAdmin && !h.isUserBotAdmin(m.Author.ID) {
		return false, 0
	}
	return true, 0
}

// Run is the main entry point to the dcmd router and is the method to call on the messageCreate event
func (h *RefHandler) Run(s *discordgo.Session, m *discordgo.MessageCreate) error {
	if h.awaitHandler(m) {
		return nil
	}

	var err error
	var a = etc.CheckPrefix(m.Content, h.Cfg.Prefixes)
	if a < 0 {
		return nil
	}

	args := etc.SplitArgs(m.Content[a:], " ")
	fmt.Println(args)

	ctx := Context{
		Ses:  s,
		Mes:  m,
		Args: args,
		Han:  h,

		Guild: &Guild{
			// this is dumb but i'm to lazy to figure out how to do it better
			ID: func() string { i, _ := h.Cache.IDChannelGuild(m.ChannelID); return i }(),
		},
	}

	c, mc := h.FetchCmd(args)

	if mc.Code == con.NoExist {
		return nil
	}

	if a, b := h.CheckPermissions(m, c.Permissions()); !a {
		fmt.Println(a, b)
		return h.respondPermError(ctx, c.Options(), etc.IntToStr(b))
	}

	ctx.Primaries = mc.Primaries
	ctx.Command = c

	switch mc.Code {
	case con.Default:
		err = defaultRouter(mc.Args, ctx, c)
	case con.DefaultRoot:
		err = defaultRouter(mc.Args, ctx, h)
	case con.Execute:
		err = c.execute(ctx, mc.Args)
	}

	if err != nil {
		if strings.HasPrefix(err.Error(), con.PermissionError) {
			return h.respondPermError(ctx, c.Options(), con.PermissionError)
		}
		h.respondError(ctx, c.Options(), err)
		h.LogError(err)
	}

	if c.parentReactOnOptions().Success.State() {
		ctx.ReactReply(h.Cfg.SuccessEmote)
	}

	return err
}

func (h *RefHandler) respondPermError(ctx Context, o CmdOptions, code string) error {
	if etc.BOverride(h.Cfg.ReactOn.Denied, o.ReactOn.Denied) {
		fmt.Println(h.Cfg.PermissionErrorEmote)
		ctx.ReactReply(h.Cfg.PermissionErrorEmote)
	}
	if etc.BOverride(h.Cfg.ReplyPermissionErrors, o.ReplyPermissionErrors) {
		m, err := ctx.XReplySprint("Error, Insuffecient Permissions to do that, code: ", code)
		if h.Cfg.DeletePermErrorMessages {
			go ctx.DeleteMessageAfterDelay(m, h.Cfg.AutoDeleteDelay)
		}
		return err
	}
	return nil
}

func (h *RefHandler) respondError(ctx Context, o CmdOptions, err error) error {
	if etc.BOverride(h.Cfg.ReactOn.Error, o.ReactOn.Error) {
		ctx.ReactReply(h.Cfg.ErrorEmote)
	}
	if etc.BOverride(h.Cfg.ReplyErrors, o.ReplyErrors) {
		m, err := ctx.XReplySprint("Error\n```\n", err, "\n```")
		if h.Cfg.DeleteErrorMessages {
			go ctx.DeleteMessageAfterDelay(m, h.Cfg.AutoDeleteDelay)
		}
		return err
	}
	return nil
}

func (h *RefHandler) AddAtMentionAsPrefix() {
	at := "<@" + h.session.State.User.ID + ">"
	h.Cfg.Prefixes = append(h.Cfg.Prefixes, at)
	at = "<@!" + h.session.State.User.ID + ">"
	h.Cfg.Prefixes = append(h.Cfg.Prefixes, at)
}

func (h *RefHandler) LogError(err error) {
	td := etc.GetTimeDateString()
	fmt.Println(td, "| Error > ", err)
	h.LogWebhook(err.Error(), "LOG: **ERROR**", 0xE91E63)
}

func (h *RefHandler) LogNormal(data ...interface{}) {
	td := etc.GetTimeDateString()
	msg := fmt.Sprint(data...)
	fmt.Println(td, "| Normal > ", msg)
	h.LogWebhook(msg, "LOG: Normal", 0x2ECC71)
}

func (h *RefHandler) LogWebhook(text, title string, color int) {
	if !h.webhookset() {
		fmt.Println("SetWH")
		return
	}
	msg := []*discordgo.MessageEmbed{&discordgo.MessageEmbed{
		Title:       title,
		Description: text,
		Timestamp:   etc.GetRFC3339DateString(),
		Color:       color,
	}}
	whmsg := discordgo.WebhookParams{
		Embeds: msg,
	}
	_, err := h.session.WebhookExecute(h.lwebh.ID, h.lwebh.Token, false, &whmsg)
	if err != nil {
		fmt.Println(err)
	}
}

func (h *RefHandler) webhookset() bool {
	if h.lwebh == nil {
		if h.Cfg.GlobalLogWebhook == "" {
			return false
		}
		wh, err := h.session.Webhook(h.Cfg.GlobalLogWebhook)
		if err != nil {
			fmt.Println(err)
			return false
		}
		h.lwebh = wh
	}
	return true
}

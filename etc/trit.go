package etc

// BOverride Compare a list of trit values in order of last to first and returns the state of the first one it finds to be set
// Note if none are set it always returns false
func BOverride(trits ...Trit) bool {
	for i := len(trits) - 1; i >= 0; i-- {
		if trits[i].IsSet() {
			return trits[i].State()
		}
	}
	return false
}

// BOverrideRev Compare a list of trit values in order of first to last and returns the state of the first one it finds to be set
// Note if none are set it always returns false
func BOverrideRev(trits ...Trit) bool {
	for _, t := range trits {
		if t.IsSet() {
			return t.State()
		}
	}
	return false
}

// BAllTrue Takes a bunch of trit values and checks to see if all set ones are true, if so it returns true, if any are false it returns false
func BAllTrue(trits ...Trit) bool {
	for _, t := range trits {
		if t.IsSet() {
			if !t.State() {
				return false
			}
		}
	}
	return true
}

// BAllFalse Takes a bunch of trit values and checks to see if all set ones are false, if so it returns true, if any are true it returns false
func BAllFalse(trits ...Trit) bool {
	for _, t := range trits {
		if t.IsSet() {
			if t.State() {
				return false
			}
		}
	}
	return true
}

// TOverride Compare a list of trit values in order of last to first and returns the state of the first one it finds to be set
// Note if none are set it always returns false
func TOverride(trits ...Trit) Trit {
	for i := len(trits) - 1; i >= 0; i-- {
		if trits[i].IsSet() {
			return trits[i]
		}
	}
	return 0
}

// TOverrideRev Compare a list of trit values in order of first to last and returns the state of the first one it finds to be set
// Note if none are set it always returns false
func TOverrideRev(trits ...Trit) Trit {
	for _, t := range trits {
		if t.IsSet() {
			return t
		}
	}
	return 0
}

// Trit , Basically an int with some methods to emulate a 3 state bool, or more accurately a bool with an null/unset state
// Primary Purpose is for Setting overrides, such as overriding a global setting on a command level
// 0 = Unset
// 1 = False
// 2 = True
type Trit uint8

// Consts used for quick setting of a trit value
const (
	STrue  = 2
	SFalse = 1
)

// IsSet , A method on Trit that returns whether or not the value is set or not
func (t Trit) IsSet() bool {
	if t != 0 {
		return true
	}
	return false
}

// State , A method that returns whether or not it is true or false
// NOTE, This does not account for whether or not it is in a "set" state, as such you'll need to check for that yourself via Trit.IsSet()
func (t Trit) State() bool {
	if t == 2 {
		return true
	}
	return false
}

// Set , A method that sets the state of Trit, not entirely needed since you can set it just like an int value without the methods but it exists as an abstraction
func (t Trit) Set(s bool) {
	if s {
		t = 2
	} else {
		t = 1
	}
}

// UnSet , Sets it to 0, or sets it as "unset"
func (t Trit) UnSet() {
	t = 0
}

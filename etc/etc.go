package etc

import (
	"strconv"
	"strings"
	"time"
)

// IntToStr pretty much just a shortcut to strconv.Itoa
func IntToStr(i int) string {
	return strconv.Itoa(i)
}

// Between returns the contents of a string between before and after, yes its messy but it works
func Between(s, after, before string) (string, bool) {
	if strings.Index(s, after) >= 0 && strings.Index(s, before) > 0 {
		return s[strings.Index(s, after)+len(after) : strings.Index(s, before)], true
	}
	return s, false
}

// Prepend Function
// Basically like the default append function but for prepending
func Prepend(s string, r []string) []string {
	a := make([]string, len(r)+1)
	a[0] = s
	for i := range r {
		a[i+1] = r[i]
	}
	return a
}

// GetRFC3339DateString , Returns the current time in RFC3339 Standardised Format
func GetRFC3339DateString() string {
	t := time.Now()
	return t.Format(time.RFC3339)
}

// GetTimeDateString , Returns a string of the date and time using GetDateString and GetTimeString seperated by an underscore
func GetTimeDateString() string {
	return GetDateString() + "_" + GetTimeString()
}

// GetTimeString , Returns a string of the current time in 24h HH:MM:SS format
func GetTimeString() string {
	t := time.Now()
	return t.Format("15:04:05")
}

// GetDateString , Returns a string of the Current date in YYYY-MM-DD format
func GetDateString() string {
	t := time.Now()
	return t.Format("2006-1-2")
}

// CheckPrefix Function
// Checks if a message contains any of the prefixes listen in a string array
// Returns the length of the prefix if true, returns -1 if false
func CheckPrefix(text string, prefix []string) int {
	text = strings.ToLower(text)
	for p := range prefix {
		if strings.HasPrefix(text, prefix[p]+" ") {
			return len(prefix[p]) + 1
		}
		if strings.HasPrefix(text, prefix[p]) {
			return len(prefix[p])
		}
	}
	return -1
}

// SplitArgs Function
// A function used to split arguments (roughly based off of strings.Split but with support for quotes mutalated in)
// Code here is kinda crappy/ugly but it works:tm:
// Sorry in advanced to any future reader who has to figure this mess out
func SplitArgs(s, sep string) []string {
	n := strings.Count(s, sep) + 1
	a := make([]string, n)
	n--
	i := 0
	for i < n {
		m := strings.Index(s, sep)
		if m < 0 {
			break
		}
		if s[0] == byte('"') && strings.Count(s, "\"") > 1 {
			bb := s[1:]
			b := bb[:strings.Index(bb, "\"")]
			n = n - strings.Count(b, " ")
			s = s[len(b)+2:]
			if len(s) != 0 {
				if s[0] == ' ' {
					s = s[1:]
				}
			}
			a[i] = b
		} else {
			a[i] = s[:m]
			s = s[m+len(sep):]
		}
		i++
	}
	a[i] = s
	return a[:n+1]
}

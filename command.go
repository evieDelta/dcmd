package dcmd

import (
	"strings"

	con "codeberg.org/eviedelta/dcmd/consts"
	"codeberg.org/eviedelta/dcmd/etc"
)

// TODO figure out which bits of shenanigans can be removed / cleaned up / unexported
// TODO Go through and fix/update any comments that is incomplete/outdated/useless

// RefCommand default struct for command data
type RefCommand struct {
	CName     string      // The name of the command
	Desc      string      // A short description of the command used by the help command
	Man       []string    // More in depth command help used by the manual command
	Perms     Permissions // The permissions the command requires (TODO implement discord permissions)
	Opts      CmdOptions  // The various options of the command
	Aliasof   Cmd         // Method 1 of 2 for setting aliases, this one works by linking the command object directly
	TextAlias string      // Method 2 or 2 for setting aliases, this one works by taking a command name and calling the handler for that command

	Function func(Context, []string) error // The guts and logic of the command

	// Private stuff
	sub         map[string]bool // Do subcommands exist?
	subCommands cmdMap
	handler     *RefHandler
	parent      Cmd
}

// AddCommand is fairly self explainatory, takes a command object and add it as a subcommand to the command
func (c *RefCommand) AddCommand(cmd Cmd) {
	cmd.parentCommand(c)
	if c.sub == nil {
		c.sub = make(map[string]bool)
	}
	if c.subCommands == nil {
		c.subCommands = make(map[string]Cmd)
	}
	c.sub[cmd.Name()] = true
	c.subCommands[cmd.Name()] = cmd
}

// ListCommands Lists the subcommands
func (c *RefCommand) ListCommands() []string {
	return c.subCommands.commandList()
}

// DefaultCfg returns the setting about default commands
func (c *RefCommand) DefaultCfg() DefaultOptions {
	return c.Opts.Default
}

// Name returns the Name value
func (c *RefCommand) Name() string {
	return c.CName
}

// Description returns the command Description
func (c *RefCommand) Description() string {
	return c.Desc
}

// Manual returns a page of the manual
func (c *RefCommand) Manual(page int) string {
	return c.Man[page]
}

// ManualPages returns the number of manual pages the command has
func (c *RefCommand) ManualPages() int {
	return len(c.Man)
}

// Permissions returns the permission data of the command
func (c *RefCommand) Permissions() Permissions {
	return c.Perms
}

// Options returns the option data of the command
func (c *RefCommand) Options() CmdOptions {
	return c.Opts
}

// Commands returns the entire data of the subcommands list
func (c *RefCommand) commands() *cmdMap {
	return &c.subCommands
}

// FetchCmd similar to the handlers FetchCmd Method, Recursively searches down the chain of commands/subcommands until it hits an endpoint, where it then returns that layer
func (c *RefCommand) fetchCmd(key []string) (Cmd, msgReturn) {
	//fmt.Println("Command FetchCMD On: ", c.Name())
	// Checks if the command is an alias to another command, if so route through that
	// Possible TODO?/Idea? change how aliases are handled so aliases get checked by the layer below instead of checking themselves
	// (eg the handler fetchCmd method checks if the command is an alias, or the command fetchCmd checks if the subcommand is an alias, instead of it checking if itself is an alias)
	// thought behind this is so settings like Opts.DataSubCommand don't have to be mirrored on both the main command and its aliases
	if b, bb := c.checkAlias(key); b != nil {
		//fmt.Println("Command Check Alias")
		return b, bb
	}

	// Checks if the there are no more layers to the arguments, if so than it ends the chain and returns the current layer, aka itself
	if len(key) == 0 {
		//fmt.Println("Command no more arguments, returning for execution")
		return c, msgReturn{Code: con.Execute, Args: key}
	}

	// Checks if the next argument is a default command, if so than it it returns itself with the action code marking that it should run the default router
	if chkDefault(key, c.parentDefaultOptions()) {
		//fmt.Println("Command Builtin")
		return c, msgReturn{Code: con.Default, Args: key}
	}

	// Checks if the next argument is an existant subcommand
	// If the above check denotes there it is an existant subcommand, than it continues the chain by running the fetch method onto that layer
	ke := strings.ToLower(key[0])
	if c.DoesCommandExist(ke) {
		//fmt.Println("Command Subcommand exists: ", ke)
		if !c.subCommands[ke].Options().RequirePrimary {
			//fmt.Println("Command subcommand doesn't require primary, executing Fetch Method on subcommand")
			x := *c.commands()
			return x[ke].fetchCmd(key[1:])
		}
	}

	// Is second layer subcommands enabled? if so and there is enough arguments then check if that is an existant subcommand
	// if all is enabled than it'll continue the chain through here, and return the argument prior as a special case called primaries
	if len(key) >= 2 {
		//fmt.Println("command more arguments")
		ke := strings.ToLower(key[1])
		if c.DoesCommandExist(ke) {
			//fmt.Println("command layer 2 subcommand exists, checking")
			if c.subCommands[ke].Options().DataSubcommand {
				//fmt.Println("command layer 2 subcommand is enabled, executing check method")
				x := *c.commands()
				cmd, ret := x[ke].fetchCmd(key[2:])
				//fmt.Println("Layer 2 fetched, prepending key[0] with content ", key[0])
				ret.Primaries = etc.Prepend(key[0], ret.Primaries)
				return cmd, ret
			}
		}
	}
	//fmt.Println("command final layer returning for execution")
	// Doesn't seem like there is any more layers, so it ends the chain and returns itself
	return c, msgReturn{Code: con.Execute, Args: key}
}

// CheckAlias checks if the current layer is an alias to anything, if so than it continues the chain going into the found alias, else it just returns
func (c *RefCommand) checkAlias(key []string) (Cmd, msgReturn) {
	//fmt.Println("Command Check Alias")
	// Check if there is a pointer/direct alias, if so than run the FetchCmd method on that and continue the chain
	if c.Aliasof != nil {
		//fmt.Println("Command Object Alias")
		return c.aliasOf().fetchCmd(key)
	}
	// Check if there is a text/command name alias, if so than recontinue the chain from the start based off of that command name instead
	if c.aliasOfText() != "" {
		// Possible Bug? unconfirmed but i have a feeling that text aliases will only be able to alias to top level commands, text aliases may not be able to alias to subcommands as it is (perhaps change it so textalias is an array, so it can account for all layers down)
		//fmt.Println("Command text alias, restaring cycle")
		z := etc.Prepend(c.aliasOfText(), key)
		return c.handler.FetchCmd(z)
	}
	//fmt.Println("Command no alias found")
	// If neither of the above report an alias than it returns the scope back and doesn't become a part of the command chain
	return nil, msgReturn{}
}

// DoesCommandExist checks if there is a subcommand with the name of X string
func (c *RefCommand) DoesCommandExist(a string) bool {
	return c.sub[a]
}

// execute executes Order 66                          actually just runs the function of the command object
func (c *RefCommand) execute(ctx Context, args []string) error {
	return c.Function(ctx, args)
}

// aliasOf returns the AliasOf data, mostly just exists so you can get the data through an interface
func (c *RefCommand) aliasOf() Cmd {
	return c.Aliasof
}

// aliasOfText returns the Text alias data, mostly just exists so you can get the data through an interface
func (c *RefCommand) aliasOfText() string {
	return c.TextAlias
}

// parentHandler sets the handler the command belongs to
func (c *RefCommand) parentHandler(h *RefHandler) {
	c.handler = h
}

// referenceHandler sets the handler the command belongs to
func (c *RefCommand) parentCommand(cmd Cmd) {
	c.parent = cmd
}

func (c *RefCommand) parentDefaultOptions() DefaultOptions {
	if c.parent != nil {
		parent := c.parent.parentDefaultOptions()
		return DefaultOptions{
			DisableAll:         etc.TOverride(parent.DisableAll, c.DefaultCfg().DisableAll),
			DisableHelp:        etc.TOverride(parent.DisableHelp, c.DefaultCfg().DisableHelp),
			DisableManual:      etc.TOverride(parent.DisableManual, c.DefaultCfg().DisableManual),
			DisableListCommand: etc.TOverride(parent.DisableListCommand, c.DefaultCfg().DisableListCommand),
		}
	}
	if c.handler != nil {
		return DefaultOptions{
			DisableAll:         etc.TOverride(c.handler.DefaultCfg().DisableAll, c.DefaultCfg().DisableAll),
			DisableHelp:        etc.TOverride(c.handler.DefaultCfg().DisableHelp, c.DefaultCfg().DisableHelp),
			DisableManual:      etc.TOverride(c.handler.DefaultCfg().DisableManual, c.DefaultCfg().DisableManual),
			DisableListCommand: etc.TOverride(c.handler.DefaultCfg().DisableListCommand, c.DefaultCfg().DisableListCommand),
		}
	}
	return c.DefaultCfg()
}

func (c *RefCommand) parentReactOnOptions() ReactOn {
	if c.parent != nil {
		parent := c.parent.parentReactOnOptions()
		return ReactOn{
			Error:   etc.TOverride(parent.Error, c.Opts.ReactOn.Error),
			Denied:  etc.TOverride(parent.Denied, c.Opts.ReactOn.Denied),
			Success: etc.TOverride(parent.Success, c.Opts.ReactOn.Success),
			Failure: etc.TOverride(parent.Failure, c.Opts.ReactOn.Failure),
		}
	}
	if c.handler != nil {
		return ReactOn{
			Error:   etc.TOverride(c.handler.Cfg.ReactOn.Error, c.Opts.ReactOn.Error),
			Denied:  etc.TOverride(c.handler.Cfg.ReactOn.Denied, c.Opts.ReactOn.Denied),
			Success: etc.TOverride(c.handler.Cfg.ReactOn.Success, c.Opts.ReactOn.Success),
			Failure: etc.TOverride(c.handler.Cfg.ReactOn.Failure, c.Opts.ReactOn.Failure),
		}
	}
	return c.Opts.ReactOn
}

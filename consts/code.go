package con

// Const Values for the Return codes on MsgReturn
const (
	NoExist = 0
	Execute = 1 + iota
	Default
	DefaultRoot
)

// Const Values for Error codes
const (
	PermissionError = "[500]"
)

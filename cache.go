package dcmd

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/patrickmn/go-cache"
)

// TODO figure out what other things could be / should be cached
// TODO figure out what is already cached by discordgos session.State and use that for those things

// Cache interface used for accessing the cache system
/*type iCache interface {
	ObjChannelFromID(string) (*discordgo.Channel, bool)
	ObjGuildFromID(string) (*discordgo.Guild, bool)

	IDChannelGuild(string) (string, bool)
}*/

// NewCache returns a new cache
func newCache(dg *discordgo.Session) *pCache {
	p := pCache{
		Channels: cache.New(5*time.Minute, 5*time.Minute),
		Guilds:   cache.New(5*time.Minute, 5*time.Minute),

		ChannelGuild: cache.New(cache.NoExpiration, cache.NoExpiration),

		dg: dg,
	}
	return &p
}

type pCache struct {
	Channels *cache.Cache
	Guilds   *cache.Cache

	ChannelGuild *cache.Cache

	dg *discordgo.Session
}

func (c *pCache) IDChannelGuild(channel string) (string, bool) {
	if ch, found := c.ChannelGuild.Get(channel); found {
		return ch.(string), true
	}
	if ch, found := c.ObjChannelFromID(channel); found {
		c.ChannelGuild.Add(ch.ID, ch.GuildID, cache.DefaultExpiration)
		return ch.GuildID, true
	}
	return "", false
}

func (c *pCache) ObjChannelFromID(channel string) (*discordgo.Channel, bool) {
	if ch, found := c.Channels.Get(channel); found {
		return ch.(*discordgo.Channel), true
	}
	ch, err := c.dg.State.Channel(channel)
	if err != nil {
		fmt.Println(err)
		return ch, false
	}
	c.Channels.Add(ch.ID, ch, cache.DefaultExpiration)
	return ch, true
}

func (c *pCache) ObjGuildFromID(guild string) (*discordgo.Guild, bool) {
	if gl, found := c.Guilds.Get(guild); found {
		return gl.(*discordgo.Guild), true
	}
	gl, err := c.dg.State.Guild(guild)
	if err != nil {
		fmt.Println(err)
		return gl, false
	}
	c.Channels.Add(gl.ID, gl, cache.DefaultExpiration)
	return gl, true
}

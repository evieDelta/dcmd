package dcmd

// NewComMap Function
// Returns an empty ComMap Type
func newCmdMap() cmdMap {
	return make(map[string]Cmd)
}

// cmdMap map type with a string key and a command interface
// Used for storing the commands and subcommands
type cmdMap map[string]Cmd

// Description returns the description of a command contained in the list
func (c cmdMap) description(cmd string) string {
	x := c[cmd]
	return x.Description()
}

// Manual returns a manual page of a command contained in the list
func (c cmdMap) manual(cmd string, page int) string {
	x := c[cmd]
	return x.Manual(page)
}

// CommandList returns a list of all commands contained in this command list recursively checking sublevels as well
func (c cmdMap) commandList() []string {
	out := make([]string, 0)
	for _, x := range c {
		if x.Options().EnableCommandsList {
			out = append(out, x.Name())
			if x.Options().ListSubCommands && len(*x.commands()) > 0 {
				y := x.commands().commandList()
				for i := range y {
					out = append(out, x.Name()+"/"+y[i])
				}
			}
		}
	}
	return out
}

package dcmd

import (
	//"fmt"
	"strconv"
	"strings"

	"codeberg.org/eviedelta/dcmd/etc"
)

// TODO, allow users to find help on commands when the default is run at root (eg, `!help echo` as opposed to `!echo help`)
// Possible TODO? Rewrite the default commands system to be regular commands/subcommands instead of their own subsystem
// TODO rename default commands to BuiltIn commands

// DefaultOptions , a struct containing options on whether or not to enable the default subcommands on a message (or handler) object
type DefaultOptions struct {
	DisableAll         etc.Trit
	DisableManual      etc.Trit
	DisableHelp        etc.Trit
	DisableListCommand etc.Trit
}

// ChkDefault , a function that checks if a subcommand is a default command
// and also checks if both sets of the DefaultOptions struct allow that default command to be run
func chkDefault(args []string, cn DefaultOptions) bool {
	if cn.DisableAll.State() {
		return false
	}
	arg := strings.ToLower(args[0])
	if arg == "help" && !cn.DisableHelp.State() {
		return true
	}
	if arg == "manual" && !cn.DisableManual.State() {
		return true
	}
	if arg == "commands" && !cn.DisableListCommand.State() {
		return true
	}
	return false
}

// DefaultRouter , Checks which default command is present in the first argument and executes it
func defaultRouter(args []string, ctx Context, c comData) error {
	switch args[0] {
	case "help":
		return help(args, ctx, c)
	case "manual":
		return manual(args, ctx, c)
	case "commands":
		return list(args, ctx, c)
	}
	return nil
}

// help , Default command for returning the help of a ComData object
func help(args []string, ctx Context, c comData) error {
	if len(args) > 1 {
		return manual(args, ctx, c)
	}
	return ctx.ReplySimple(c.Description())
}

// manual , Default manual command
func manual(args []string, ctx Context, c comData) error {
	// Parse the page number argument if provided
	var a int64
	if len(args) > 1 {
		var err error
		a, err = strconv.ParseInt(args[1], 0, 32)
		if err != nil {
			return ctx.ReplySimple("Invalid Page number provided, please provide a page in the form of a decimal number")
		}
	}
	a--

	// If the command has no pages than return saying there is no manual command
	if c.ManualPages() <= 0 {
		return ctx.ReplySimple("No manual found for command: " + c.Name())
	}
	// If page number isn't provided (it'll be 0 in that case) or is greater than the number of pages than provide the first page
	if a < 1 || int(a) >= c.ManualPages() {
		return ctx.ReplySimple(c.Manual(0))
	}
	// if everything matches up and a seems to be a page number, than return that page
	return ctx.ReplySimple(c.Manual(int(a)))
}

func list(args []string, ctx Context, c comData) error {
	var s string
	for _, x := range c.ListCommands() {
		s = s + "\n" + x
	}
	return ctx.ReplySprint(len(s), "\n```\n", s, "\n```")
}

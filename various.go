package dcmd

//import (
//"fmt"

//"github.com/bwmarrin/discordgo"
//"strings"
//"gitea.pi.lan/evie/gosystemkit/etc"
//)

// TODO make type Guild and type Member actually useful

// AliasPlaceholder a completely blank command existing solely for use as a placeholder in alias commands
// Not needed with how the command handler works, but in the off case something glitches this can be used to make sure it doesn't crash trying to run the function on an alias
func AliasPlaceholder(ctx Context, args []string) error {
	return nil
}

// Guild is a struct containing quick grab info about the guild for use within the Context type
type Guild struct {
	ID string
}

// Member is a struct containing quick grab info about a member for use within the Context type
type Member struct {
}

// Possible TODO, if default commands are adapted to be able to just be regular commands / subcommands then comData could be removed

// ComData Primarly used as a container for the data the default/help commands might need
type comData interface {
	Description() string
	Manual(int) string
	ManualPages() int

	Name() string

	DefaultCfg() DefaultOptions

	ListCommands() []string
}

// MsgReturn a type used for the fetchCmd method to return the action for the handler to take with the returned command
// States are contained in the con subpackage
type msgReturn struct {
	Code int
	Args []string

	Primaries []string
}

//used for ctx.AwaitNextMessage() to check status
type awaitState struct {
	channel chan AwaitData
	done    bool
}

func genAwaitString(author, channel string) string {
	return author + ":" + channel
}

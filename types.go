package dcmd

import (
	"codeberg.org/eviedelta/dcmd/etc"
	"github.com/bwmarrin/discordgo"
)

// TODO Fully Implement Permissions
// TODO Rename all Configuration types to be more consistant ( possibly prefix with Cfg ? )
// Possible TODO rewrite all the 3 state / etc.Trit / overridable settings to be in their own "Inharitables" struct which can be used in both the handler, and the commands/subcommands

// Possible idea? remove the Cmd interface and just have everything work off of the RefCommand struct (which would be renamed to just Command then)

// Version , the version of the library
const Version = "v0.0.0.3-alpha"

// AwaitData contains the data that'll be given by ctx.AwaitNextMessage()
// includes the message in argument form, if the await has been canceled (from either the cancel command or a timeout)
// and the raw message data
type AwaitData struct {
	Args   []string
	Mes    *discordgo.Message
	Cancel bool
}

// CmdOptions Struct Type
// Contains various options for commands to use
type CmdOptions struct {
	Default DefaultOptions

	// Sort of an odd option, allows checking an extra layer deep for a subcommand and takes the argument prior to the subcommand as the primary arg
	// Ex !command data subcommand
	DataSubcommand bool
	RequirePrimary bool // Require above? (for cases where a command is to be operated only on a specific arg, similar to pk;member [member] option)

	EnableCommandsList bool // Show this command in the commands list?
	ListSubCommands    bool // Show this commands subcommands in the commands list?

	ReactOn               ReactOn  // Contexts it should emote react to an initiating message on
	ReplyPermissionErrors etc.Trit // Reply to a message if it runs into a permission error
	ReplyErrors           etc.Trit // Reply to a message if it errors
}

// Permissions Struct containing various permission flags used by the bot to make sure a user has appropriate permissions
type Permissions struct {
	BotAdmin      bool
	Administrator bool // TODO
	ManageServer  bool // TODO
	KickMembers   bool // TODO
	Discord       int  // TODO
}

// ReactOn Settings struct to define whether to react to a message on certain events such as permission denied, or success
// currently only PermissionDenied and Error is supported
type ReactOn struct {
	Success etc.Trit
	Error   etc.Trit
	Denied  etc.Trit
	Failure etc.Trit // TODO? (how would this be implemented, currently as is there isn't a way to return a "failed" flag beyond errors as covered by the above)
}

// Cmd Interface Type
// Interface used for defining the methods needed by the command handler
type Cmd interface {
	// Static Info about the command, including description, manual, and manual page count
	Name() string
	Description() string
	Manual(int) string
	ManualPages() int

	// The options related to a command
	Permissions() Permissions
	Options() CmdOptions
	DefaultCfg() DefaultOptions // Default commands enable/disable

	// Does a subcommand exist?
	DoesCommandExist(string) bool

	// Add a command
	AddCommand(Cmd)

	// grab a list of all subcommands
	ListCommands() []string

	// Private functions

	// Get the map of subcommands
	commands() *cmdMap

	// Execute the command
	execute(Context, []string) error

	// Fetch a subcommand, or itself if no subcommand is present
	fetchCmd([]string) (Cmd, msgReturn)

	// Grab the data relating to which command this command is an alias to
	// (in either Cmd format or just the name of the command, note however that its not gaurenteed for both to be defined)
	aliasOf() Cmd
	aliasOfText() string

	// add a reference to the handler to a command (used when a command is added to a handler)
	parentHandler(*RefHandler)
	parentCommand(Cmd)

	parentDefaultOptions() DefaultOptions
	parentReactOnOptions() ReactOn
}

module codeberg.org/eviedelta/dcmd

go 1.13

require (
	github.com/bwmarrin/discordgo v0.20.2
	github.com/gorilla/websocket v1.4.1 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad // indirect
	golang.org/x/sys v0.0.0-20200117145432-59e60aa80a0c // indirect
)

package dcmd

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/patrickmn/go-cache"
)

// TODO add more helper methods onto Context

// Context contains various data about the message and context a command was called from, as well as containing a variety of helper methods
type Context struct {
	// The raw discordgo data, here in case there is something not covered by dcmd or if there is a need to access discordgo directly
	Ses *discordgo.Session
	Mes *discordgo.MessageCreate
	// The handler which this command was called from, in case you need to call a handler function directly
	Han *RefHandler
	// The command object of the command being run, likely not needed outside of odd cases but is here if you may need it
	Command Cmd

	// Arguments, the stuff sent after the command
	Args []string
	// Primaries, if you have a subcommand configured to run after an argument (eg, !command data subcommand) this will contain the argument preceeding the subcommand
	Primaries []string

	// TODO, figure out and add information from the context to here so you don't need to do it manually
	// #### NOTE MOST OF THIS IS TODO AND IS NOT IMPLEMENTED YET (guild ID is added though)
	Guild  *Guild
	Member *Member
}

// ReplySimple simplest reply method, simply takes a string and replies it to the context
func (c *Context) ReplySimple(content string) error {
	_, err := c.Ses.ChannelMessageSend(c.Mes.ChannelID, content)
	return err
}

// XReplySimple like ReplySimple except also returns the message object
func (c *Context) XReplySimple(content string) (*discordgo.Message, error) {
	msg, err := c.Ses.ChannelMessageSend(c.Mes.ChannelID, content)
	return msg, err
}

// ReplySprint simple reply method using fmt.Sprint to be able to take any data
// similar to fmt.Println except it outputs as a reply on discord
func (c *Context) ReplySprint(content ...interface{}) error {
	_, err := c.Ses.ChannelMessageSend(c.Mes.ChannelID, fmt.Sprint(content...))
	return err
}

// XReplySprint like ReplySprint except also returns the message object
func (c *Context) XReplySprint(content ...interface{}) (*discordgo.Message, error) {
	msg, err := c.Ses.ChannelMessageSend(c.Mes.ChannelID, fmt.Sprint(content...))
	return msg, err
}

// ReactReply adds a reaction to the message which triggered this context instance
func (c *Context) ReactReply(s string) error {
	return c.Ses.MessageReactionAdd(c.Mes.ChannelID, c.Mes.ID, s)
}

// DeleteMessageAfterDelay deletes a message after a delay in milliseconds
func (c *Context) DeleteMessageAfterDelay(m *discordgo.Message, t int) {
	d := time.Duration(t) * time.Second
	time.Sleep(d)
	c.Ses.ChannelMessageDelete(m.ChannelID, m.ID)
}

// AwaitNextMessage returns a channel and tells the handler to send the next message sent by the author through the channel instead of passing it through the usual command handler
func (c *Context) AwaitNextMessage() chan AwaitData {
	ch := make(chan AwaitData)
	x := genAwaitString(c.Mes.Author.ID, c.Mes.ChannelID)

	c.Han.await.Add(x, &awaitState{channel: ch}, cache.DefaultExpiration)
	return ch
}

// AwaitUntilNextMessage like AwaitNextMessage() except it blocks and returns the message contents directly after a message is recived
// basically is shorthand for ch := ctx.AwaitNextMessage(); data := <-ch
func (c *Context) AwaitUntilNextMessage() AwaitData {
	ch := make(chan AwaitData)
	x := genAwaitString(c.Mes.Author.ID, c.Mes.ChannelID)

	c.Han.await.Add(x, &awaitState{channel: ch}, cache.DefaultExpiration)
	return <-ch
}
